#include <18F2620.h>
#device ADC=10

#FUSES NOWDT                    //No Watch Dog Timer
//#FUSES WDT128                   //Watch Dog Timer uses 1:128 Postscale
//#FUSES NORSS                    //No secure segment RAM
#FUSES BORV43                   //Brownout reset at 4.3V
#FUSES NOXINST                  //Extended set extension and Indexed Addressing mode disabled (Legacy mode)
#FUSES NOWRTC                   //Configuration registers not write protected
#FUSES NOWRTB                   //Boot block not write protected
#FUSES LP

#use delay(clock=4000000)
//#use rs232(baud=115200,parity=N,xmit=PIN_C7,rcv=PIN_C6,bits=8,stream=PORT1)
#use rs232(UART1, BAUD=19200, RECEIVE_BUFFER=64, TRANSMIT_BUFFER=64, TXISR)
#define USE_TX_ISR

#define C2 PIN_B1

#use PWM(FREQUENCY=1000, OUTPUT=PIN_C2, BITS=10, DUTY=0)

