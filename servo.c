#include <servo.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#include "input.c"

char *int32_to_4_bytes(unsigned int32 value);
void print_bytes(char *bytes, int8 n);

signed int32 position;
signed int32 setpoint_value;
signed int32 integral;
signed int32 derivative;
signed int32 output;
signed int32 error;
signed int32 previous_error;
unsigned int16 pwm_percent;

#define Kp 5
#define Ki 0
#define Kd 0
#define dt 0.065536 

/* PID algorithm
previous_error = 0
integral = 0
loop:
  error = setpoint - measured_value
  integral = integral + error * dt
  derivative = (error - previous_error) / dt
  output = Kp * error + Ki * integral + Kd * derivative
  previous_error = error
  wait(dt)
  goto loop
*/

#INT_TIMER0
void TIMER0_isr(void)
{
    previous_error = error; 
    error = setpoint_value - position;
    integral = integral + error * dt;
    derivative = (error - previous_error) / dt;
    output = Kp * error + Ki * integral + Kd * derivative;
    
    if(output > 0){
        output_high(PIN_C3);
    } else {
        output_low(PIN_C3);
    }

/*
    if (abs(error) < 100) {
        pwm_set_duty_percent(0);
    } else {

    }
*/
    if(abs(output) > 999) {
        pwm_percent = 999;
    } else {
    pwm_percent = (int16)abs(output);
    }
    
    pwm_set_duty_percent(pwm_percent);
    

}
#INT_EXT
void  EXT_isr(void)  // triggered by the *INT0* pin
{
   if(input(C2)) {
      position++;
   }
   else {
      position--;
   }
}
/*
#INT_EXT1
void  EXT1_isr(void) 
{

}
*/
char *int32_to_4_bytes(unsigned int32 value)
{
    static char bytes[4];
    
    bytes[0] = (value >> 24) & 0xFF;
    bytes[1] = (value >> 16) & 0xFF;
    bytes[2] = (value >> 8) & 0xFF;
    bytes[3] = value & 0xFF;
    
    return bytes;
}

// prints n bytes, including NULL values
void print_bytes(char *bytes, int8 n)
{
    for(int8 i = 0; i < n; i++)
        putc(bytes[i]);
}

void main()
{
    // each 65.536 ms for 4 MHz clock
    setup_timer_0(RTCC_INTERNAL|RTCC_DIV_256|RTCC_8_bit); 
                            
    enable_interrupts(GLOBAL);
    ext_int_edge(INT_EXT, H_TO_L);
    // ext_int_edge(INT_EXT1, H_TO_L);
    
    printf("Initialized.\r\n");
    //print_bytes(int32_to_4_bytes(0), 4);
    //put(0);
    //printf("%s", int32_to_4_bytes(0xab));
    
    position = 0;
    enable_interrupts(INT_TIMER0);
    enable_interrupts(INT_EXT);
    //pwm_set_duty_percent(950);
    setpoint_value = 1000;
    
    // Serial communication reception buffer
    char rx_buffer[65]; 
    
    enum commands{SETPOINT = 0, STATUS = 1};
    
    struct Message{
       char  command[2];
       char  parameter[10];
    } message;  
    
    while(TRUE)
    {
        if(kbhit())
        {
            get_string(rx_buffer, 64);
            //printf("%s", rx_buffer);
            //putc(rx_buffer[0] - );
            //putc('\r');
            
            int command = rx_buffer[0] - '0'; // convert digit to int
            switch(command)
            {
                case SETPOINT:
                setpoint_value = atol(rx_buffer);
                printf("%ld", setpoint_value);
                break;
                
                case STATUS:
                printf("%ld", setpoint_value); putc('\r');
                printf("%ld", position); putc('\r');
                printf("%ld", integral); putc('\r');
                printf("%ld", derivative); putc('\r');
                break;
            }
            rx_buffer[0] = '\0';
        }
            
    }
}

